import React, {useState} from 'react';
import { useHistory, useLocation } from "react-router-dom";
import Actions from '../state_management/Actions';

function LoginPage() {
  let history = useHistory();
  let location = useLocation();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");


  let { from } = location.state || { from: { pathname: "/" } };

  let handleAuthentication = async () => {
    fetch("http://localhost:4000/api/auth/identity/callback", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        user: {
          username: username,
          password: password
        }
      })
    }).then( response => response.json()
    ).then( json => {
      console.log( json.data );
      Actions.login( username, json.data.token );
      history.replace(from);
    });
  }
    return (
      <div>
        <p>You must log in to view the page at {from.pathname}</p>
        <input value={username} onChange={ev => setUsername(ev.target.value)} />
        <input value={password} onChange={ev => setPassword(ev.target.value)} />
        <button onClick={() => handleAuthentication()}>Log in</button>
      </div>
    );
}

export default LoginPage;