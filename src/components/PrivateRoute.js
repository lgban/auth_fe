import React from 'react';
import Reflux from 'reflux';
import {Route, Redirect } from "react-router-dom";
import AppStore from '../state_management/AppStore';

class PrivateRoute extends Reflux.Component {
  constructor(props) {
    super(props);
    this.store = AppStore;
  }
  render() {
    return (
      <Route
        render={({ location }) => {
          return (
              this.state.authenticated ?
                (
                  this.props.children
                ) : (
                  <Redirect
                    to={{
                      pathname: "/login",
                      state: { from: location }
                    }}
                  />
                )
            )
          }
        }
      />
    );
  }
}

export default PrivateRoute;