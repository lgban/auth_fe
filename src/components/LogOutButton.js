import React from 'react';
import Reflux from 'reflux';
import Actions from '../state_management/Actions';
import AppStore from '../state_management/AppStore';

class LogOutButton extends Reflux.Component {
  constructor(props) {
    super(props);
    this.store = AppStore;
  }
  render() {
    return this.state.authenticated ? (
      <p>
        Welcome '{this.state.username}'!{" "}
        <button
          onClick={() => {
            Actions.logout();
          }}
        >
          Log out
        </button>
      </p>
    ) : null;
  }
}

export default LogOutButton;