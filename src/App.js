import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import PrivateRoute from './components/PrivateRoute';
import LoginPage from './components/LoginPage';
import LogOutButton from './components/LogOutButton';
import ProtectedPage from './components/ProtectedPage';

class App extends React.Component {
  render() {
    return (
    <Router>
      <LogOutButton />
      <div>
        <ul>
          <li>
            <Link to="/public">Public Page</Link>
          </li>
          <li>
            <Link to="/protected">Protected Page</Link>
          </li>
        </ul>

        <Switch>
          <Route path="/public">
            <div>Public</div>
          </Route>
          <Route path="/login">
            <LoginPage />
          </Route>
          <PrivateRoute path="/protected">
            <ProtectedPage />
          </PrivateRoute>
        </Switch>
      </div>
    </Router>
    );
  }
}

export default App;